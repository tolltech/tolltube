﻿using System;
using TollTube.DomainLayer.Common;
using TollTube.RepositoryLayer.Google;

namespace TollTube.DomainLayer
{
    public class WhiskeyDriveFile
    {
        private readonly Func<byte[]> getBody;
        public DateTime? Date { get; }

        private readonly string Title;
        private byte[] Body;

        public WhiskeyDriveFile(DriveFileDto driveFileDto, Func<byte[]> getBody)
        {
            this.getBody = getBody;
            Title = driveFileDto.Name;
            Date = DateTimeHelpers.ExtractRussianDateTime(Title);
        }

        public byte[] GetFileBody()
        {
            return Body ?? (Body = getBody());
        }
    }
}