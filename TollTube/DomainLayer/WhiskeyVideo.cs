﻿namespace TollTube.DomainLayer
{
    public class WhiskeyVideo
    {
        private readonly WhiskeyDriveFile driveFile;
        private readonly WhiskeyWikiPage wikiPage;

        public WhiskeyVideo(WhiskeyDriveFile driveFile, WhiskeyWikiPage wikiPage)
        {
            this.driveFile = driveFile;
            this.wikiPage = wikiPage;
        }

        public bool ReadyToUpload => driveFile != null && wikiPage != null && wikiPage.ReadyToUpload;


        public string Description => wikiPage.GetBodyView();    
        public string Title => wikiPage.Title.Replace("[ready]", "").Trim();
        public byte[] VideoBytes => driveFile.GetFileBody();

        public WhiskeyWikiPage MarkUploaded()
        {
            return wikiPage.MarkUploaded();
        }
    }
}