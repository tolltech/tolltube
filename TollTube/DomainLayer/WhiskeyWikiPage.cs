﻿using System;
using TollTube.DomainLayer.Common;
using TollTube.RepositoryLayer.Wiki;

namespace TollTube.DomainLayer
{
    public class WhiskeyWikiPage
    {
        private readonly Func<WikiPageBody> getPageBody;
        private const string UploadedLabel = "[uploaded]";
        private const string ReadyLabel = "[ready]";

        public DateTime? Date { get; }

        public readonly string Title;
        public readonly string Id;
        private WikiPageBody PageBody { get; set; }

        private WhiskeyWikiPage(string title, string id)
        {
            Title = title;
            Id = id;
        }

        public WhiskeyWikiPage(WikiPageLight wikiPage, Func<WikiPageBody> getPageBody)
        {
            this.getPageBody = getPageBody;
            Id = wikiPage.Id;
            Title = wikiPage.Title;
            Date = DateTime.TryParse(Title.Trim().SafeSubString(10), out var parsedDate) ? (DateTime?)parsedDate : null;
        }

        public string GetBodyView()
        {
            return GetBody().View.Value;
        }

        public string GetBodyStorage()
        {
            return GetBody().Storage.Value;
        }

        private WikiPageBody GetBody()
        {
            return PageBody ?? (PageBody = getPageBody());
        }

        public bool ReadyToUpload => Title.Contains(ReadyLabel);

        public WhiskeyWikiPage MarkUploaded()
        {
            return new WhiskeyWikiPage($"{Title.Replace(ReadyLabel, string.Empty)} {UploadedLabel}", Id)
            {
                PageBody = this.PageBody
            };
        }
    }
}