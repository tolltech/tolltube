﻿namespace TollTube.RepositoryLayer.Google
{
    public interface IDriveQueryBuilder
    {
        IDriveQueryBuilder InFolder(string folderId);
        string ToQueryString();
    }
}