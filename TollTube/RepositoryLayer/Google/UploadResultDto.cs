﻿using System;

namespace TollTube.RepositoryLayer.Google
{
    public class UploadResultDto
    {
        public bool Success { get; set; }
        public Exception Exception { get; set; }
        public string VideoId { get; set; }
    }
}