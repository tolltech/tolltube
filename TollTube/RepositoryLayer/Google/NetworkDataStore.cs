﻿using System;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Google.Apis.Util.Store;
using Newtonsoft.Json;

namespace TollTube.RepositoryLayer.Google
{
    public class NetworkDataStore : IDataStore
    {
        private readonly string googleAccessToken;
        private readonly string cryptoKey;
        private readonly CryptoService cryptoService;

        private const string regNameKey = @"TollTube";

        public NetworkDataStore(string googleAccessToken, string cryptoKey)
        {
            this.cryptoKey = cryptoKey;
            this.googleAccessToken = googleAccessToken;
            cryptoService = new CryptoService();
        }

        public async Task StoreAsync<T>(string key, T value)
        {
            var seralizedAuth = JsonConvert.SerializeObject(value);
            var crypted = cryptoService.Encrypt(seralizedAuth, cryptoKey);
            using (var client = new WebClient())
            {
                var valueStr = await client
                    .DownloadStringTaskAsync($"https://tolltech.ru/study/Find?key={regNameKey}_{key}")
                    .ConfigureAwait(false);
                var existent = JsonConvert.DeserializeObject<KeyValue>(valueStr);

                var serializeObject = JsonConvert.SerializeObject(new KeyValue
                {
                    Key = $"{regNameKey}_{key}",
                    Value = crypted
                });
                var bodyBytes = Encoding.UTF8.GetBytes(serializeObject);
                if (existent != null)
                {
                    await client
                        .UploadDataTaskAsync($"https://tolltech.ru/study/UpdateFromBody",
                            bodyBytes).ConfigureAwait(false);
                }
                else
                {
                    await client
                        .UploadDataTaskAsync($"https://tolltech.ru/study/Create",
                            bodyBytes).ConfigureAwait(false);
                }
            }
        }

        public Task DeleteAsync<T>(string key)
        {
            using (var client = new WebClient())
            {
                return client.UploadDataTaskAsync($"https://tolltech.ru/study/Update?key={regNameKey}_{key}&value=none",
                    Array.Empty<byte>());
            }
        }

        public async Task<T> GetAsync<T>(string key)
        {
            using (var client = new WebClient())
            {
                var valueStr = await client
                    .DownloadStringTaskAsync($"https://tolltech.ru/study/Find?key={regNameKey}_{key}")
                    .ConfigureAwait(false);

                var value = JsonConvert.DeserializeObject<KeyValue>(valueStr)?.Value;

                if (value == "none")
                {
                    value = string.Empty;
                }

                if (key == "billing.kontur" && string.IsNullOrWhiteSpace(value))
                {
                    //note: этот хак нужен для того чтобы на сервере не вспылвало окно OAuth. Токен нужно получить заранее
                    var accessToken = googleAccessToken;
                    return JsonConvert.DeserializeObject<T>(accessToken);
                }

                try
                {
                    var decryptedValue = cryptoService.Decrypt(value, cryptoKey);
                    return JsonConvert.DeserializeObject<T>(decryptedValue);
                }
                catch (Exception e)
                {
                    await Console.Error.WriteLineAsync($"Fail to deserialize google token by key {key}. {e.Message}")
                        .ConfigureAwait(false);
                    return default;
                }
            }
        }

        public Task ClearAsync()
        {
            return Task.CompletedTask;
        }

        private class KeyValue
        {
            public string Key { get; set; }
            public string Value { get; set; }
        }
    }
}