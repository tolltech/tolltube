﻿namespace TollTube.RepositoryLayer.Google
{
    public interface ICryptoService
    {
        string Encrypt(string src, string cryptoKey);

        string Decrypt(string src, string cryptoKey);
    }
}