﻿using System.Threading.Tasks;
using Google.Apis.Util.Store;
using Microsoft.Win32;
using Newtonsoft.Json;

namespace TollTube.RepositoryLayer.Google
{
    public class RegistryDataStore : IDataStore
    {
        private readonly string googleAccessToken;
        private readonly string cryptoKey;
        private readonly CryptoService cryptoService;

        private const string regNameKey = @"SOFTWARE\TollTube2";


        public RegistryDataStore(string googleAccessToken, string cryptoKey)
        {
            this.googleAccessToken = googleAccessToken;
            this.cryptoKey = cryptoKey;
            cryptoService = new CryptoService();
        }

        public Task StoreAsync<T>(string key, T value)
        {
            var regKey = Registry.CurrentUser.CreateSubKey(regNameKey);
            var seralizedAuth = JsonConvert.SerializeObject(value);
            var crypted = cryptoService.Encrypt(seralizedAuth, cryptoKey);
            regKey.SetValue(key, crypted);
            regKey.Close();
            return Task.CompletedTask;
        }

        public Task DeleteAsync<T>(string key)
        {
            var regKey = Registry.CurrentUser.OpenSubKey(regNameKey, true);
            regKey?.DeleteSubKey(key, false);
            return Task.CompletedTask;
        }

        public Task<T> GetAsync<T>(string key)
        {
            var regKey = Registry.CurrentUser.OpenSubKey(regNameKey);
            var value = regKey?.GetValue(key)?.ToString();
            regKey?.Close();

            if (key == "billing.kontur" && value == null)
            {
                //note: этот хак нужен для того чтобы на сервере не вспылвало окно OAuth. Токен нужно получить заранее
                var accessToken = googleAccessToken;
                return Task.FromResult(JsonConvert.DeserializeObject<T>(accessToken));
            }

            var decryptedValue = cryptoService.Decrypt(value, cryptoKey);
            return Task.FromResult(JsonConvert.DeserializeObject<T>(decryptedValue));
        }

        public Task ClearAsync()
        {
            Registry.CurrentUser.DeleteSubKey(regNameKey);
            return Task.CompletedTask;
        }
    }
}