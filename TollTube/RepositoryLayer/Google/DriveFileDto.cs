﻿namespace TollTube.RepositoryLayer.Google
{
    public class DriveFileDto
    {
        public string Name { get; set; }
        public string FileId { get; set; }
    }
}