﻿namespace TollTube.RepositoryLayer.Google
{
    public class YoutubeVideoDto
    {
        public string Name { get; set; }
        public string VideoId { get; set; }
    }
}