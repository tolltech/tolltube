﻿using System.Threading.Tasks;

namespace TollTube.RepositoryLayer.Google
{
    public interface ICloudShare
    {
        byte[] DownloadFile(string fileId);
        Task<DriveFileDto[]> GetFilesAsync(string folderId);
        Task<UploadResultDto> UploadToYouTubeAsync(byte[] fileBytes, VideoMeta videoMeta, string channelId);
    }
}