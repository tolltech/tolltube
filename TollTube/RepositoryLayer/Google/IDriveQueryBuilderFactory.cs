﻿namespace TollTube.RepositoryLayer.Google
{
    public interface IDriveQueryBuilderFactory
    {
        IDriveQueryBuilder Create();
    }
}