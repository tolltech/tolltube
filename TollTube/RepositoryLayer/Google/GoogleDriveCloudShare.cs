﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Upload;
using Google.Apis.Util.Store;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;

namespace TollTube.RepositoryLayer.Google
{
    public class GoogleDriveCloudShare : ICloudShare
    {
        private readonly string apiKey;
        private readonly string clientSecret;
        private readonly IDriveQueryBuilderFactory driveQueryBuilderFactory;
        private readonly string googleAccessToken;
        private readonly string cryptoKey;
        private readonly bool useNetworkStore;

        public GoogleDriveCloudShare(string apiKey,
            string clientSecret,
            IDriveQueryBuilderFactory driveQueryBuilderFactory, string googleAccessToken, string cryptoKey, bool useNetworkStore = false)
        {
            this.apiKey = apiKey;
            this.clientSecret = clientSecret;
            this.driveQueryBuilderFactory = driveQueryBuilderFactory;
            this.googleAccessToken = googleAccessToken;
            this.cryptoKey = cryptoKey;
            this.useNetworkStore = useNetworkStore;
        }

        private DriveService CreateDriveService() => new DriveService(new BaseClientService.Initializer
        {
            ApplicationName = "Treller",
            ApiKey = apiKey
        });

        private async Task<YouTubeService> CreateYouTubeServiceAsync()
        {
            UserCredential credential;
            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(clientSecret)))
            {
                Console.WriteLine($"Try authorize to Youtube");

                credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    new[]
                    {
                        YouTubeService.Scope.Youtube, YouTubeService.Scope.YoutubeUpload,
                        YouTubeService.Scope.Youtubepartner, "https://www.googleapis.com/auth/plus.login",
                        "https://www.googleapis.com/auth/userinfo.email"
                    },
                    "billing.kontur",
                    CancellationToken.None,
                    useNetworkStore 
                        ? (IDataStore) new NetworkDataStore(googleAccessToken, cryptoKey) 
                        : new RegistryDataStore(googleAccessToken, cryptoKey)
                ).ConfigureAwait(false);

                Console.WriteLine($"Success authorize to Youtube");
            }

            return new YouTubeService(new BaseClientService.Initializer
            {
                ApplicationName = "Treller",
                HttpClientInitializer = credential
            });
        }

        public byte[] DownloadFile(string fileId)
        {
            using (var driveService = CreateDriveService())
            {
                using (var memoryStream = new MemoryStream())
                {
                    driveService.Files.Get(fileId).Download(memoryStream);
                    return memoryStream.ToArray();
                }
            }
        }

        public async Task<DriveFileDto[]> GetFilesAsync(string folderId)
        {
            using (var driveService = CreateDriveService())
            {
                var request = driveService.Files.List();
                request.Q = driveQueryBuilderFactory.Create().InFolder(folderId).ToQueryString();
                var result = await request.ExecuteAsync().ConfigureAwait(false);
                request.Fields = "createdTime";
                return result.Files
                    .Select(x => new DriveFileDto
                    {
                        Name = x.Name,
                        FileId = x.Id
                    })
                    .ToArray();
            }
        }

        public async Task<UploadResultDto> UploadToYouTubeAsync(byte[] fileBytes, VideoMeta videoMeta, string channelId)
        {
            using (var youTubeService = await CreateYouTubeServiceAsync().ConfigureAwait(false))
            {
                var video = new Video
                {
                    Snippet = new VideoSnippet
                    {
                        Title = videoMeta.Title,
                        Description = videoMeta.Description,
                        Tags = new[] { "nightwhiskey", "whiskeytube", "konturbilling" },
                        ChannelId = channelId,
                        CategoryId = "22", // See https://developers.google.com/youtube/v3/docs/videoCategories/list
                    },
                    Status = new VideoStatus
                    {
                        PrivacyStatus = "unlisted", // or "private" or "public"
                    },
                };

                using (var memoryStream = new MemoryStream(fileBytes))
                {
                    Console.WriteLine($"Going to upload meta of {videoMeta.Title} to youtube");

                    var request = youTubeService.Videos.Insert(video, "snippet,status", memoryStream, "video/*");

                    Console.WriteLine($"Uploaded meta of {videoMeta.Title} to youtube");

                    string videoId = null;
                    request.ResponseReceived += x => videoId = x.Id;

                    Console.WriteLine($"Going to upload {videoMeta.Title} to youtube");

                    var progress = await request.UploadAsync().ConfigureAwait(false);

                    Console.WriteLine($"Just uploaded {videoMeta.Title} to youtube");

                    return new UploadResultDto
                    {
                        Exception = progress.Exception,
                        Success = progress.Status == UploadStatus.Completed,
                        VideoId = videoId
                    };
                }
            }
        }       
    }
}