﻿namespace TollTube.RepositoryLayer.Google
{
    public class VideoMeta
    {
        public string Title { get; set; }
        public string Description { get; set; }
    }
}