﻿namespace TollTube.RepositoryLayer.Wiki
{
    public interface IWikiClient
    {
        WikiPage GetPage(string pageId);
        WikiPageLight[] GetChildren(string pageId);
        WikiPage UpdateAndGetNewPage(string pageId, string newTitle, string newBody);
    }
}