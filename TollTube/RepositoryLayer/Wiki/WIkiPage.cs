﻿using Newtonsoft.Json;

namespace TollTube.RepositoryLayer.Wiki
{
    public class WikiPage : WikiPageLight
    {
        [JsonProperty("body")]
        public WikiPageBody Body { get; set; }
    }

    public class WikiPageLight
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("version")]
        public WikiPageVersion Version { get; set; }
        [JsonProperty("space")]
        public WikiPageSpace Space { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
    }

    public class WikiPageBody
    {
        [JsonProperty("storage")]
        public WikiPageBodyStorage Storage { get; set; }
        [JsonProperty("view")]
        public WikiPageBodyStorage View { get; set; }
    }

    public class WikiPageBodyStorage
    {
        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("representation")]
        public string Representation { get; set; }
    }

    public class WikiPageSpace
    {
        [JsonProperty("key")]
        public string Key { get; set; }
    }

    public class WikiPageVersion
    {
        [JsonProperty("number")]
        public int Number { get; set; }
    }

    public class WikiPageSearchResult
    {
        public WikiPageLight[] Results { get; set; }
    }
}