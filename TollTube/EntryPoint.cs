﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Serialization;
using TollTube.ApplicationLayer;
using TollTube.Configuration;
using TollTube.DomainLayer.Common;
using TollTube.RepositoryLayer.Google;
using TollTube.RepositoryLayer.Wiki;

namespace TollTube
{
    public static class EntryPoint
    {
        private static async Task Main(params string[] args)
        {
            try
            {
                var googleApiKey = args[0];
                var googleSpreadsheetsCredentials = args[1];
                var wikiLogin = args[2];
                var wikiPassword = args[3];
                var googleAccessToken = args[4];
                var credentials = new Credentials(wikiLogin, wikiPassword, googleSpreadsheetsCredentials, googleApiKey);
                var cryptoKey = SafeGetArgs(args, 8, string.Empty);
                var videoCount = int.TryParse(SafeGetArgs(args, 9, string.Empty), out var parsed) ? parsed : (int?)null;
                var useNetworkStore = bool.TryParse(SafeGetArgs(args, 10, string.Empty), out var parsedUseNewtowrkStore) && parsedUseNewtowrkStore;
                var whiskeyTubeService = new WhiskeyTubeService(
                    new GoogleDriveCloudShare(credentials.GoogleApiKey, credentials.GoogleSpreadsheetsCredentials,
                        new DriveQueryBuilderFactory(), googleAccessToken, cryptoKey, useNetworkStore),
                    new WikiClient(new JsonSerializer(), credentials.WikiCredentials.AuthHeader));

                Console.WriteLine($"Starting Tolltube with " +
                                  $"wikiLogin {wikiLogin} " +
                                  $"wikiPassword {wikiPassword.SafeSubString(3)} " +
                                  $"googleApiKey {googleApiKey.SafeSubString(5)} " +
                                  $"googleCredentials {googleSpreadsheetsCredentials.SafeSubString(5)} " +
                                  $"cryptoKey {cryptoKey.SafeSubString(3)} " +
                                  $"googleAccessToken {googleAccessToken.SafeSubString(20)}");

                var wikiArchivePageId = SafeGetArgs(args, 5, "156696999");
                var driveFolderId = SafeGetArgs(args, 6, "17K6Nj556UL2ylNYzh2lXPpYzq7Bif8tl");
                var youtubeChannelId = SafeGetArgs(args, 7, "UCiGKUGNeK8-KHPpRqxZoYcw");

                var results = await whiskeyTubeService
                    .SyncByWikiAsync(wikiArchivePageId, driveFolderId, youtubeChannelId, videoCount).ConfigureAwait(false);

                var success = results.Where(x => x.Success).ToArray();
                var errors = results.Where(x => !x.Success).ToArray();

                Console.WriteLine($"{results.Count(x => x.Success)} videos were uploaded. \r\n" +
                                  $"{results.Count(x => !x.Success)} videos were failed. \r\n" +
                                  $"Success {string.Join(", ", success.Select(x => x.VideoId))}\r\n" +
                                  $"Errors {string.Join(", ", errors.Select(x => x.Exception.Message))}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.Error.WriteLine($"Error in whiskeyTube app {e.Message}");
                Console.Error.WriteLine($"Stack {e.StackTrace}");
            }
        }

        static string SafeGetArgs(string[] args, int index, string defaultValue)
        {
            return args.Length > index ? args[index] : defaultValue;
        }
    }
}