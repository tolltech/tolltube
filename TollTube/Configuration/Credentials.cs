﻿using System;
using System.Text;

namespace TollTube.Configuration
{
    public class Credentials
    {
        //    "GoogleSpreadsheetsCredentials": "{
        //    'installed':
        //    {
        //        'client_id':'27278181893-cf2....apps.googleusercontent.com',
        //        'project_id':'treller-188702',
        //        'auth_uri':'https://accounts.google.com/o/oauth2/auth',
        //        'token_uri':'https://accounts.google.com/o/oauth2/token',
        //        'auth_provider_x509_cert_url':'https://www.googleapis.com/oauth2/v1/certs',
        //        'client_secret':'qpo...',
        //        'redirect_uris':['urn:ietf:wg:oauth:2.0:oob','http://localhost']
        //    }
        //   }",
        //  "GoogleApiKey": "AIza..."

        public Credentials(string wikiLogin, string wikiPassword, string googleClientSecret, string googleApiKey)
        {
            GoogleApiKey = googleApiKey;
            GoogleSpreadsheetsCredentials = googleClientSecret;
            WikiCredentials = new WikiCredential(wikiLogin, wikiPassword);
        }

        public string GoogleSpreadsheetsCredentials { get; set; }
        public string GoogleApiKey { get; set; }

        public WikiCredential WikiCredentials { get; set; }
    }

    public class WikiCredential
    {
        public WikiCredential(string login, string password)
        {
            Login = login;
            Password = password;
        }

        private string Login { get; }
        private string Password { get; }

        public string AuthHeader => Convert.ToBase64String(Encoding.UTF8.GetBytes($"{Login}:{Password}"));
    }
}