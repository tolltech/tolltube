﻿using System.Threading.Tasks;
using TollTube.RepositoryLayer.Google;

namespace TollTube.ApplicationLayer
{
    public interface IWhiskeyTubeService
    {
        Task<UploadResultDto[]> SyncByWikiAsync(string wikiArchivePageId, string driveFolderId, string youtubeChannelId, int? videoCount = null);
    }
}