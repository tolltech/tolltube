﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TollTube.DomainLayer;
using TollTube.DomainLayer.Common;
using TollTube.RepositoryLayer.Google;
using TollTube.RepositoryLayer.Wiki;

namespace TollTube.ApplicationLayer
{
    public class WhiskeyTubeService : IWhiskeyTubeService
    {
        private readonly ICloudShare cloudShare;
        private readonly IWikiClient wikiClient;

        public WhiskeyTubeService(ICloudShare cloudShare, IWikiClient wikiClient)
        {
            this.cloudShare = cloudShare;
            this.wikiClient = wikiClient;
        }

        public async Task<UploadResultDto[]> SyncByWikiAsync(string wikiArchivePageId, string driveFolderId, string youtubeChannelId, int? videoCount)
        {
            var pages = wikiClient.GetChildren(wikiArchivePageId);
            var wikiPages = pages
                .Select(x => new WhiskeyWikiPage(x, () => wikiClient.GetPage(x.Id).Body))
                .ToArray();

            Console.WriteLine($"Found {wikiPages.Length} wiki pages");

            var driveFiles = (await cloudShare.GetFilesAsync(driveFolderId).ConfigureAwait(false))
                .Select(x => new WhiskeyDriveFile(x, () => cloudShare.DownloadFile(x.FileId)))
                .ToArray();

            Console.WriteLine($"Found {driveFiles.Length} google drive files");

            var videoSet = new WhiskeyVideoSet()
                .AddWikiPages(wikiPages)
                .AddDriveFiles(driveFiles);

            var videos = videoSet.GetVideosToUpload();

            Console.WriteLine($"Found {videos.Length} new video files. Get first {videoCount ?? videos.Length} of them");

            if (videoCount.HasValue && videoCount.Value < videos.Length)
            {
                videos = videos.Take(videoCount.Value).ToArray();
            }

            var result = new List<UploadResultDto>();
            foreach (var video in videos)
            {
                var uploadResult = await UploadAsync(video, youtubeChannelId).ConfigureAwait(false);
                result.Add(uploadResult);
            }

            return result.ToArray();
        }

        private const string yuotubeVideoHrefPrefix = @"https://www.youtube.com/watch?v=";

        private async Task<UploadResultDto> UploadAsync(WhiskeyVideo video, string channelId)
        {
            var videoMeta = new VideoMeta
            {
                Title = video.Title.SafeSubString(100),
                Description = $"{video.Title}\r\n\r\n{video.Description.FromHtml()}"
            };

            Console.WriteLine($"Start download {videoMeta.Title} video from google drive");

            var videoBytes = video.VideoBytes;

            Console.WriteLine($"Start upload {videoMeta.Title} video to channel {channelId}");

            var uploadResult = await cloudShare.UploadToYouTubeAsync(videoBytes, videoMeta, channelId).ConfigureAwait(false);

            Console.WriteLine($"Finish upload {videoMeta.Title} video to channel {channelId}");

            if (uploadResult.Success)
            {
                Console.WriteLine($"Success upload {videoMeta.Title}. Try mark uploaded...");

                var newPage = video.MarkUploaded();
                wikiClient.UpdateAndGetNewPage(newPage.Id, newPage.Title, $"{newPage.GetBodyStorage()}<p><a href=\"{yuotubeVideoHrefPrefix}{uploadResult.VideoId}\">Ссылка на видео</a></p>");

                Console.WriteLine($"Mark uploaded {newPage.Id}. New title {newPage.Title}");
            }
            else
            {
                Console.WriteLine($"Fail upload {videoMeta.Title} video to channel {channelId}");
            }

            return uploadResult;
        }

    }
}