﻿using System.Text.RegularExpressions;
using System.Web;
using HtmlAgilityPack;

namespace TollTube.ApplicationLayer
{
    public static class HtmlStringHelpers
    {
        public static string FromHtml(this string source)
        {
            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(source);

            return HttpUtility.HtmlDecode(Regex.Replace(htmlDocument.ParsedText, "<.*?>", "\r\n")).Replace("\r\n\r\n", "\r\n");
        }
    }
}