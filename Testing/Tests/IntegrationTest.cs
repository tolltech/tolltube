﻿using TollTube.Configuration;

namespace Testing.Tests
{
    public class IntegrationTest
    {
        protected Credentials credentials;

        protected IntegrationTest()
        {
            var wikiLogin = "todo";
            var wikiPassword = "todo";
            var googleClientSecret = @"todo";
            var googleApiKey = "todo";
            credentials = new Credentials(wikiLogin, wikiPassword, googleClientSecret, googleApiKey);
        }
    }
}