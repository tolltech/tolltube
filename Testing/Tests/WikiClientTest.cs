﻿using System;
using Serialization;
using TollTube.RepositoryLayer.Wiki;
using Xunit;

namespace Testing.Tests
{
    public class WikiClientTest : IntegrationTest
    {
        [Fact]
        public void AbleToWikiGetPage()
        {
            var wikiClient = new WikiClient(new JsonSerializer(), credentials.WikiCredentials.AuthHeader);
            var page = wikiClient.GetPage("156696999");
            Assert.NotEmpty(page.Body.Storage.Value);
            Assert.InRange(page.Version.Number, 1, int.MaxValue);
            Assert.NotEmpty(page.Title);
            Assert.NotEmpty(page.Space.Key);
            Assert.NotEmpty(page.Type);
        }

        [Fact]
        public void AbleToUpdateWikiGetPageTitle()
        {
            var wikiClient = new WikiClient(new JsonSerializer(), credentials.WikiCredentials.AuthHeader);

            var pageId = "336738258";

            var oldPage = wikiClient.GetPage(pageId);

            var newTitle = $"Сюда прилетало нло {DateTime.UtcNow}";
            var newBody = $"<p>Сюда прилетало бади нло {DateTime.UtcNow}</p>{oldPage.Body.Storage.Value}<p>Сюда прилетало бади нло {DateTime.UtcNow}</p>";

            var page = wikiClient.UpdateAndGetNewPage(pageId, newTitle, newBody);

            Assert.NotEqual(oldPage.Version.Number, page.Version.Number);
            Assert.Equal(newTitle, page.Title);
            Assert.Equal(newBody, page.Body.Storage.Value);

            wikiClient.UpdateAndGetNewPage(pageId, oldPage.Title, oldPage.Body.Storage.Value);
        }

        [Fact]
        public void AbleToWikiGetChildren()
        {
            var wikiClient = new WikiClient(new JsonSerializer(), credentials.WikiCredentials.AuthHeader);
            var children = wikiClient.GetChildren("156696999");
            Assert.NotEmpty(children);
        }

        [Fact]
        public void AbleToWikiGetChildrenNonExists()
        {
            var wikiClient = new WikiClient(new JsonSerializer(), credentials.WikiCredentials.AuthHeader);
            var children = wikiClient.GetChildren("4242442414242421");
            Assert.Empty(children);
        }

        [Fact]
        public void AbleToWikiGetPageNotExists()
        {
            var wikiClient = new WikiClient(new JsonSerializer(), credentials.WikiCredentials.AuthHeader);
            var page = wikiClient.GetPage("4242442414242421");
            Assert.Null(page);
        }
    }
}