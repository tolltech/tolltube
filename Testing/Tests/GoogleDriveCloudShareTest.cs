﻿using System.IO;
using System.Threading.Tasks;
using TollTube.RepositoryLayer.Google;
using Xunit;

namespace Testing.Tests
{
    public class GoogleDriveCloudShareTest : IntegrationTest
    {
        protected readonly ICloudShare googleDriveCloudShare;

        protected const string BillingChannelId = "UCiGKUGNeK8-KHPpRqxZoYcw";
        protected const string BillingGooglePhotoFolderId = "17K6Nj556UL2ylNYzh2lXPpYzq7Bif8tl";

        public GoogleDriveCloudShareTest()
        {
            googleDriveCloudShare = new GoogleDriveCloudShare(credentials.GoogleApiKey,
                                                              credentials.GoogleSpreadsheetsCredentials,
                                                              new DriveQueryBuilderFactory(), string.Empty, string.Empty);
        }

        [Fact]
        public void AbleToDownloadFile()
        {
            var bytes = googleDriveCloudShare.DownloadFile("1XNz2OF6xYiKPObWvBpczLx13Xossk6kR");
            Assert.True(bytes.Length > 0);
            File.WriteAllBytes("downloadResult.png", bytes);
        }

        [Fact]
        public async Task AbleToGetFilesFromFolder()
        {
            var fileNames = await googleDriveCloudShare.GetFilesAsync(BillingGooglePhotoFolderId).ConfigureAwait(false);
            Assert.True(fileNames.Length > 0);
        }
    }
}