﻿using System.Linq;
using System.Threading.Tasks;
using TollTube.RepositoryLayer.Google;
using Xunit;

namespace Testing.Tests
{
    public class SlowGoogleDriveCloudShareTest : GoogleDriveCloudShareTest
    {
        //[Fact] Залить фейковые видос маленького размера
        public async Task AbleToMoveFileToYouTube()
        {
            var files = await googleDriveCloudShare.GetFilesAsync(BillingGooglePhotoFolderId).ConfigureAwait(false);
            var videoFile = files.Single(x => x.Name.Contains("29 декабря 2017"));
            var bytes = googleDriveCloudShare.DownloadFile(videoFile.FileId);

            var videoToUpload = new VideoMeta
            {
                Title = "TEST - " + videoFile.Name,
                Description = "Test NightWhiskey"
            };

            var result = await googleDriveCloudShare.UploadToYouTubeAsync(bytes, videoToUpload, BillingChannelId).ConfigureAwait(false);
            Assert.True(result.Success, result.Exception?.Message);
        }
    }
}